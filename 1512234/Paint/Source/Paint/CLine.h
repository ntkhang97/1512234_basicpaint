#pragma once
#include "CShape.h"

class CLine :public virtual CShape
{
public:
	CLine(POINT pStart, POINT pEnd)
	{
		pSta = pStart;
		pCur = pEnd;
	}

	void Draw(HDC hdc);
};