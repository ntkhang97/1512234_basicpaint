#pragma once
#include "stdafx.h"
#include "Paint.h"
//Interface
class CShape
{
protected:
	POINT pSta;
	POINT pCur;
public:
	virtual void Draw(HDC hdc) = 0;
};