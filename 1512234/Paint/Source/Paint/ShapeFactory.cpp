#include "ShapeFactory.h"

CShape* ShapeFactory::GetSharp(DWORD ID, POINT p1, POINT p2)
{
	CShape* s = NULL;
	switch (ID)
	{
	case ID_SHAPES_RECTANGLE:
		s = new CRectangle(p1, p2);
		storage.push_back(s);
		return s;
	case ID_SHAPES_CIRCLE:
		s = new CCircle(p1, p2);
		storage.push_back(s);
		return s;
	case ID_SHAPES_LINE:
		s = new CLine(p1, p2);
		storage.push_back(s);
		return s;
	case ID_SHAPES_ELLIPSE:
		s = new CEllipse(p1, p2);
		storage.push_back(s);
		return s;
	case ID_SHAPES_SQUARE:
		s = new CSquare(p1, p2);
		storage.push_back(s);
		return s;
	default:
		return NULL;
		break;
	}
}