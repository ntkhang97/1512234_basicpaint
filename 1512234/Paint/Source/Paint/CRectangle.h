#include"CShape.h"

class CRectangle : public virtual CShape
{
public:
	CRectangle(POINT pStart, POINT pEnd)
	{
		pSta = pStart;
		pCur = pEnd;
	}

	CRectangle()
	{
		pSta.x = 0;
		pSta.y = 0;
		pCur.x = 10;
		pCur.y = 10;
	}

	virtual void Draw(HDC hdc);

	~CRectangle();
};