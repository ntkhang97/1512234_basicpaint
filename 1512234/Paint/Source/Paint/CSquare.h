#pragma once
#include "CRectangle.h"

class CSquare : public CRectangle
{
public:
	CSquare(POINT pStart, POINT pEnd)
	{
		pSta = pStart;
		pCur = pEnd;
	}

	CSquare()
	{
		pSta.x = 0;
		pSta.y = 0;
		pCur.x = 10;
		pCur.y = 10;
	}

	void Draw(HDC hdc);

	~CSquare();
};