#include<string>
#include"CCircle.h"
#include "CSquare.h"
#include "CLine.h"
#include <vector>

class ShapeFactory
{
	std::vector<CShape*> storage;
public:
	//Return pointer that hold the shape is identify by ID.
	CShape* GetSharp(DWORD ID, POINT p1, POINT p2);

	//Return the vector that hold the shapes.
	std::vector<CShape*> getStorage() { return storage; }

	//Free all storage's shapes.
	~ShapeFactory()
	{
		storage.clear();
	}
};