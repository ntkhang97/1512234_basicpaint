#pragma once
#include "CShape.h"
class CEllipse : public virtual CShape
{

public:
	CEllipse() {}
	CEllipse(POINT pStart, POINT pEnd)
	{
		pSta = pStart;
		pCur = pEnd;
	}

	virtual void Draw(HDC hdc);
	~CEllipse();
};