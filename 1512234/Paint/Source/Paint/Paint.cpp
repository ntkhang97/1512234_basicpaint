﻿// Paint.cpp : Defines the entry point for the application.
// Author: Nguyen Thinh Khang - 1512234
// Draw rectangle, line, ellipse
// Pressing shift, draw square, cirle

#include"ShapeFactory.h"
#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
POINT pStart;									// started point and current point.
POINT pCur;										
BOOL flagFade = FALSE;							// If the flagFade = TRUE, draw the fade lines.
DWORD idMenuPrev = 0;							// Id of previous menu that it was checked.
DWORD idMenuCur = 0;							// If of current checked menu.
DWORD idDraw = 0;								// Id to Draw
ShapeFactory shapeFactory;						// External Factory Object.
RECT clienRect;									
HDC hdcMem;
HBITMAP hbmMem;
HANDLE hOld;

int win_width, win_height;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void DrawFade(HWND hWnd, POINT start, POINT end, DWORD id);
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_PAINT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PAINT));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PAINT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_PAINT);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
	case WM_COMMAND:
	{
		HMENU hMenu = GetMenu(hWnd);
		int wmId = LOWORD(wParam);

		//Set the previous checked menu into unchecked.
		CheckMenuItem(hMenu, idMenuPrev, MF_UNCHECKED);

		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_SHAPES_RECTANGLE:
			CheckMenuItem(hMenu, ID_SHAPES_RECTANGLE, MF_CHECKED);
			idMenuPrev = ID_SHAPES_RECTANGLE;
			idMenuCur = ID_SHAPES_RECTANGLE;
			break;
		case ID_SHAPES_CIRCLE:
			CheckMenuItem(hMenu, ID_SHAPES_CIRCLE, MF_CHECKED);
			idMenuPrev = ID_SHAPES_CIRCLE;
			idMenuCur = ID_SHAPES_CIRCLE;
			break;
		case ID_SHAPES_ELLIPSE:
			CheckMenuItem(hMenu, ID_SHAPES_ELLIPSE, MF_CHECKED);
			idMenuPrev = ID_SHAPES_ELLIPSE;
			idMenuCur = ID_SHAPES_ELLIPSE;
			break;
		case ID_SHAPES_LINE:
			CheckMenuItem(hMenu, ID_SHAPES_LINE, MF_CHECKED);
			idMenuPrev = ID_SHAPES_LINE;
			idMenuCur = ID_SHAPES_LINE;
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdcMain;

		hdcMain = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...

		//Draw all of shapes in main DC
		SelectObject(hdcMain, GetStockObject(NULL_BRUSH));
		std::vector<CShape*> vtS = shapeFactory.getStorage();
		for (int i = 0; i < vtS.size(); i++)
			vtS[i]->Draw(hdcMain);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		//No menu was selected.
		if (idMenuCur == 0)
			break;

		//Set the started point and the current point.
		pCur.x = LOWORD(lParam);
		pStart.x = LOWORD(lParam);

		pCur.y = HIWORD(lParam);
		pStart.y = HIWORD(lParam);
		flagFade = TRUE;
	}
	break;
	case WM_LBUTTONUP:
	{

		if (idMenuCur == 0)
			break;

		//Save all shape in the storage and draw them on main DC.
		flagFade = FALSE;
		HDC hdc;
		hdc = GetDC(hWnd);
		SelectObject(hdc, GetStockObject(NULL_BRUSH));
		CShape* shape = shapeFactory.GetSharp(idDraw, pStart, pCur);
		InvalidateRect(hWnd, NULL, TRUE);
	}
	break;
	case WM_MOUSEMOVE:
	{
		idDraw = idMenuCur;
		if (flagFade == TRUE)
		{
			BOOL shiftFlag = 0;
			if (wParam & MK_SHIFT)
				shiftFlag = 1;

			pCur.x = LOWORD(lParam);
			pCur.y = HIWORD(lParam);

			DWORD shpe_width = abs(pCur.x - pStart.x);
			DWORD shpe_height = abs(pCur.y - pStart.y);
			
			//You are pressing a shift button.
			if (shiftFlag == 1)
			{
				if (idMenuCur == ID_SHAPES_ELLIPSE)
				{
					idDraw = ID_SHAPES_CIRCLE;
				}
				else if (idMenuCur == ID_SHAPES_RECTANGLE)
				{
					idDraw = ID_SHAPES_SQUARE;
				}

				//Choosing the small size.
				if (shpe_width < shpe_height)
					pCur.y = pStart.y + pCur.x - pStart.x;
				else
					pCur.x = pStart.x + pCur.y - pStart.y;
			}

			DrawFade(hWnd, pStart, pCur, idDraw);
		}
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


void DrawFade(HWND hWnd, POINT start, POINT end, DWORD id)
{
	HDC hdc;
	hdc = GetDC(hWnd);
	GetClientRect(hWnd, &clienRect);

	//Get width and height of client rectangle.
	win_width = clienRect.right - clienRect.left;
	win_height = clienRect.bottom - clienRect.top;

	//Create DC in memory and Bitmap in memmory. 
	hdcMem = CreateCompatibleDC(hdc);
	hbmMem = CreateCompatibleBitmap(hdc, win_width, win_height);
	hOld = SelectObject(hdcMem, hbmMem);

	//Fill the client rectangle in white.
	FillRect(hdcMem, &clienRect, NULL);

	//Get an shape by the factory object.
	ShapeFactory sf;
	CShape* s = sf.GetSharp(id, start, end);

	//Draw the shape on DC memmory.
	s->Draw(hdcMem);

	//Draw all shape in new DC. Because when I created the bitmap successfully
	//It filled the client with new rectangle so my shapes was hided.
	//Therefore I had to draw them again.
	SelectObject(hdcMem, GetStockObject(NULL_BRUSH));
	std::vector<CShape*> vtS = shapeFactory.getStorage();
	for (int i = 0; i < vtS.size(); i++)
		vtS[i]->Draw(hdcMem);

	//Copty the source in hdcMem directly to hdc.
	BitBlt(hdc, 0,0, win_width, win_height, hdcMem, 0, 0, SRCCOPY);

	//Free them.
	SelectObject(hdcMem, hOld);
	DeleteObject(hbmMem);
	DeleteDC(hdcMem);
}