﻿Nguyễn Thịnh Khang
1512234

Chương trình paint cơ bản.

Các chức năng đã thực hiện:

1. Đường thẳng (line).
2. Hình chữ nhật (rectangle). Nếu giữ phím Shift sẽ vẽ hình vuông (Square).
3. Hình Ellipse. Nếu giữ phím Shift sẽ vẽ hình tròn (Circle).
4. Cho phép chọn loại hình cần vẽ (từ menu). Menu được chọn sẽ đánh dấu tích.
    Trong một thời điểm chỉ có một menu được chọn.
5. Bọc tất cả các đối tượng vào các lớp model và sử dụng đa xạ để cài đặt và quản lý 
     các đối tượng, dùng mẫu thiết kế Factory để tạo các đối tượng.
6. Xử lý được lỗi nháy khi vẽ các đối tượng.

Các luồng sự kiện chính:
1. Khi khởi động chương trình, chưa có bất kì menu nào được chọn, thao tác vẽ
    không được thực thi.
2. Click chọn một hình để vẽ trong menu Shapes, bấm và kéo thả chuột sẽ ra hình 
    muốn vẽ, bấm giữ shift sẽ vẽ các hình các (nếu có), thả shift ra sẽ vẽ lại hình ban đầu.

link youtube: https://youtu.be/HvX7S1n2Duo
link repo: https://bitbucket.org/ntkhang97/1512234_basicpaint